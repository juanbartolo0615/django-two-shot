from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
def redirect_to_home(requets):
    return redirect("home")


@login_required(login_url="/accounts/login/")
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user.id)
    context = {
        "receipts_list": receipts,
    }
    return render(request, "receipts/home.html", context)


@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required(login_url="/accounts/login/")
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_list": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required(login_url="/accounts/login/")
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account,
    }
    return render(request, "receipts/accounts.html", context)


@login_required(login_url="/accounts/login/")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}
    return render(request, "receipts/createCategory.html", context)


@login_required(login_url="/accounts/login/")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "receipts/createCategory.html", context)
